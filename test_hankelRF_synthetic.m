clear; clc;


%% Load data
load test_data_6sys_long
num_sample = size(data,1);

% NOTE: in practice each instance may have different dimension and
% different length, so each of them is in a cell.
N = 120;
T = 50;
sigma = 0.0;
data_train  = cell(1,N);
label_train = zeros(1,N);

for n=1:N
    i = randi(num_sample,1);
    data_train{n}  = data(i,1:T) + randn(size(data(i,1:T)))*sigma;
    label_train(n) = label(i);
end

data_test  = cell(1,N);
label_test = zeros(1,N);

for n=1:N
    i = randi(num_sample,1);
    data_test{n}  = data(i,1:T) + randn(size(data(i,1:T)))*sigma;
    label_test(n) = label(i);
end


%% Forest Setup
option.num_w = 256; % how many random projections are generated per node.
% option.num_feature = 2;%option.num_w; % number of features
option.numTrail = 10; % how many random thresholds are tried per node
option.HankelWidth = 10; % number of Hankel matrix columns
option.maxDepth= 16;  % maximum depth of the tree
option.minCount = 8;
option.minChild = 1; 
option.mode = {'HankelPurity'};%,'DisplacementPurity'}; % training modes
option.numTree = 15; % number of trees to train
% option.numClass = 10; % number of classes 
option.rfpts = 0; % Referance point, 0--mean; 1--end point;

%% Training Hankel RF
hrf = ExRF_train_hankel(data_train, label_train, option);

%% Testing Hankel RF
[label_pred, prob_pred] = ExRF_apply_hankel(data_test,hrf,0);
aphrf = sum(label_pred == label_test)/numel(label_test)

%% Compare with Vanilla RF
data_train_concat = cell2mat(data_train')';
data_test_concat = cell2mat(data_test')';
vrf = ExRF_train(data_train_concat, label_train, option);
[label_pred_vanilla, prob_pred] = ExRF_apply(data_test_concat,vrf,0);
apvrf = sum(label_pred_vanilla == label_test)/numel(label_test)


%% 

%% Compare with Hankelized Data Vanilla RF
% NOTE: Remember this idea came from talking with Octavia. She suggested
% hankelizing the data and trying it on regular RF might be an interesting
% comparison. That is what we are doing here. 

data_train_hankelized = cell(1,length(data_train));
data_test_hankelized = cell(1,length(data_train));
label_train_hankelized = cell(1,length(data_test));
label_test_hankelized = cell(1,length(data_test));
num_train_hankelized = zeros(1,length(data_train));
num_test_hankelized = zeros(1,length(data_test));

% this loop makes hankels for each dimension. hankels have fixed column
% number which corresponds to our time window.
for n=1:length(data_train)
    data_n = [];
    
    for d=1:size(data_train{n},1)
        data_n = [data_n; hankel_mo(data_train{n}(d,:),[0 option.HankelWidth])'];
    end
    
    data_train_hankelized{n} = data_n;
    label_train_hankelized{n} = repmat(label_train(n),[1 size(data_n,2)]);
    num_train_hankelized(n) = size(data_n,2);
end

for n=1:length(data_test)
    data_n = [];
    
    for d=1:size(data_test{n},1)
        data_n = [data_n; hankel_mo(data_test{n}(d,:),[0 option.HankelWidth])'];
    end
    
    data_test_hankelized{n} = data_n;
%     label_test_hankelized{n} = repmat(label_train(n),[1 size(data_n,2)]);
    num_test_hankelized(n) = size(data_n,2);
end

%% Train
data_train_hankelized_concat = cell2mat(data_train_hankelized);
label_train_hankelized = cell2mat(label_train_hankelized);
hvrf = ExRF_train(data_train_hankelized_concat, label_train_hankelized, option);


%% Test
data_test_hankelized_concat = cell2mat(data_test_hankelized);
[label_pred_hankelized_concat, prob_pred_hankelized_concat] = ExRF_apply(data_test_hankelized_concat,hvrf,0);

% apply aggregated majority vote
label_pred_hankelized = zeros(1,length(data_test));
cumnum_test_hankelized = cumsum(num_test_hankelized);
cumnum_test_hankelized = [0 cumnum_test_hankelized];
for n=1:length(data_test)
    nstart = cumnum_test_hankelized(n)+1;
    nend = cumnum_test_hankelized(n+1);
    
    % find the majoriy label in sublabels
    lbl = label_pred_hankelized_concat(nstart:nend);
    [m,f,c] = mode(lbl);    % nice function to find most frequent number
    label_pred_hankelized(n) = m;
end

aphvrf = sum(label_pred_hankelized == label_test)/numel(label_test)



