clear,clc;

%% Data
load MHAD_data_whole
labels = label_act;
num_sample = size(data,2);

N = 250;
idx_train = randi(num_sample,1,N);
idx_test  = randi(num_sample,1,N);
num_train = N;
num_test  = N;

data_train  = data(idx_train);
data_test   = data(idx_test);
label_train = labels(idx_train);
label_test  = labels(idx_test);

%%

%default options
options.UseDerivative = true;
% options.SVM_C = 100;
% for wHHw
options.NumRegressors = 100;
options.HankelWidth   = 30;    % number of Hankel matrix columns
options.NumDims       = 1;     % how many joint dimensions to consider

% for covMat
options.NumLevels = 2;
options.UseOverlap = true;

options.opt1 = 3;
options.opt2 = 5;
options.opt3 = 1;
options.opt4 = 0.2;

% parameter to be tested
optname = 'HankelWidth';

% parameter values to be tested
optvals = [30 40 50];

% function to be optimized
% NOTE: function should accomodate following structure
% fun(funcparams{:},options);
func = @traintest_wHHw;
funcparams = {data_train,label_train,data_test,label_test};

[err_funvals, funvals] = optimize_param(func,funcparams,options,optname,optvals);


%% plot errorbars
errorbar(optvals, err_funvals(1,:),err_funvals(2,:))
xlabel(optname);
ylabel('Accuracy');
