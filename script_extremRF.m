clc
clear 
%%
option.num_w = 256; % how many random projections are generated per node.
option.num_feature = 2;%option.num_w; % number of features
option.numTrail = 10; % how many random thresholds are tried per node
option.HankelWidth = 10; % number of Hankel matrix columns
option.maxDepth= 16;  % maximum depth of the tree
option.minCount = 8;
option.minChild = 1; 
option.mode = {'HankelPurity'};%,'DisplacementPurity'}; % training modes
option.numTree = 15; % number of trees to train
option.numClass = 10; % number of classes 
option.rfpts = 0; % Referance point, 0--mean; 1--end point;

%% data loading
data = load('exp9_n2.txt');
sub_train = randsample(size(data,1),round(size(data,1)/2));
data_train = data(sub_train,2:end)';
label_train = data(sub_train,1)';

data_test = data(~ismember([1:size(data,1)],sub_train),2:end)';
label_test = data(~ismember([1:size(data,1)],sub_train),1)';
%% training 
HRF_forest = ExRF_train(data_train, label_train, option);

%% testing
[label_pred, prob_pred] = ExRF_apply(data_test,HRF_forest,0);
ap = sum(label_pred == label_test)/numel(label_test);