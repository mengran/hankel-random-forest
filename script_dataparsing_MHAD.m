clc
clear 
%%
bvh_info = dir(fullfile('data','*.bvh'));
label_act = [];
label_sub = [];
label_rep = [];
data_x = [];
data_y = [];
data_z = [];
offset = [];
num_frame = [];
slide_win = 50;

data = cell(1,numel(bvh_info));
for b = 1:numel(bvh_info)
    b
    bvh_name = bvh_info(b).name;
    us_loc = strfind(bvh_name,'_');
    act = str2double(bvh_name(us_loc(2)+2:us_loc(3)-1));
    sub = str2double(bvh_name(us_loc(1)+2:us_loc(2)-1));
    rep = str2double(bvh_name(us_loc(3)+2:end-4));
    [skeleton,time] = loadbvh(fullfile('data',bvh_name(1:end-4)));
    curFPS = round(numel(time)/time(end));
    sub_interval = round(curFPS/30);
    % subsample frames
    num_joint = numel(skeleton);
    tmpdata_x = zeros(num_joint,numel(1:sub_interval:numel(time)));
    tmpdata_y = zeros(num_joint,numel(1:sub_interval:numel(time)));
    tmpdata_z = zeros(num_joint,numel(1:sub_interval:numel(time)));    
    
    for j = 1:numel(skeleton)
        tmpdata_x(j,:) = skeleton(j).Dxyz(1,1:sub_interval:end);
        tmpdata_y(j,:) = skeleton(j).Dxyz(3,1:sub_interval:end);
        tmpdata_z(j,:) = skeleton(j).Dxyz(2,1:sub_interval:end);        
    end
    data_tmp = zeros(size(tmpdata_x,1)*3,size(tmpdata_x,2));
    data_tmp(1:3:end,:) = tmpdata_x;
    data_tmp(2:3:end,:) = tmpdata_y;
    data_tmp(3:3:end,:) = tmpdata_z;
    data{b} = data_tmp;
    label_act(b) = act;
    label_sub(b) = sub;
    label_rep(b) = rep;
    
%---------------% sliding window extraction--------------------------%
%     data_x_tmp = zeros(num_joint*(size(tmpdata_x,2)-slide_win+1),slide_win);
%     data_y_tmp = zeros(num_joint*(size(tmpdata_x,2)-slide_win+1),slide_win);
%     data_z_tmp = zeros(num_joint*(size(tmpdata_x,2)-slide_win+1),slide_win);
%     offset_tmp = zeros(num_joint*(size(tmpdata_x,2)-slide_win+1),3);
%     label_act_tmp = ones(num_joint*(size(tmpdata_x,2)-slide_win+1),1) * act;
%     label_sub_tmp = ones(num_joint*(size(tmpdata_x,2)-slide_win+1),1) * sub;
%     label_rep_tmp = ones(num_joint*(size(tmpdata_x,2)-slide_win+1),1) * rep;
%     num_frame_tmp = zeros(num_joint*(size(tmpdata_x,2)-slide_win+1),1);
%     for sl = 1:size(tmpdata_x,2)-slide_win+1
%         data_x_tmp(num_joint*(sl-1)+1:num_joint*sl,:) = tmpdata_x(:,sl:sl+slide_win-1);
%         data_y_tmp(num_joint*(sl-1)+1:num_joint*sl,:) = tmpdata_y(:,sl:sl+slide_win-1);
%         data_z_tmp(num_joint*(sl-1)+1:num_joint*sl,:) = tmpdata_z(:,sl:sl+slide_win-1);
%         offset_tmp(num_joint*(sl-1)+1:num_joint*sl,1) = tmpdata_x(:,sl+slide_win-1)-tmpdata_x(1,sl+slide_win-1);
%         offset_tmp(num_joint*(sl-1)+1:num_joint*sl,2) = tmpdata_y(:,sl+slide_win-1)-tmpdata_y(1,sl+slide_win-1);
%         offset_tmp(num_joint*(sl-1)+1:num_joint*sl,3) = tmpdata_z(:,sl+slide_win-1)-tmpdata_z(1,sl+slide_win-1);
%         num_frame_tmp(num_joint*(sl-1)+1:num_joint*sl,:) = sl;
%     end
%     data_x = [data_x;data_x_tmp];
%     data_y = [data_y;data_y_tmp];
%     data_z = [data_z;data_z_tmp];
%     offset = [offset;offset_tmp];
%     label_act = [label_act; label_act_tmp];
%     label_sub = [label_sub; label_sub_tmp];
%     label_rep = [label_rep; label_rep_tmp];
%     num_frame = [num_frame; num_frame_tmp];
%-------------------------------------------------------------------%

end

% data_traj = zeros(size(data_x,1),size(data_x,2),3);
% data_traj(:,:,1) = data_x;
% data_traj(:,:,2) = data_y;
% data_traj(:,:,3) = data_z;
% data = cell(2);
% data{1} = data_traj;
% data{2} = offset;
% save('MHAD_data.mat','data','label_act','label_rep','label_sub','num_frame','num_joint');
save('MHAD_data_long.mat','data','label_act','label_rep','label_sub','num_joint');