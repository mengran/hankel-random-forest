clc
clear 
%%
option.num_w = 256; % how many random projections are generated per node.
option.num_feature = 2;%option.num_w; % number of features
option.numTrail = 10; % how many random thresholds are tried per node
option.HankelWidth = 10; % number of Hankel matrix columns
option.maxDepth= 16;  % maximum depth of the tree
option.minCount = 8;
option.minChild = 1; 
option.mode = {'HankelPurity'};%,'DisplacementPurity'}; % training modes
option.numTree = 15; % number of trees to train
option.numClass = 10; % number of classes 
option.rfpts = 0; % Referance point, 0--mean; 1--end point;

%% data loading
% I am going to generate random data just to check the implementation
% NOTE: each data has different time length
% D : dimension of measurements (eg. arm_x,arm_y,arm_z, ...,leg_x, leg_y,...)
% T : max length of sequence, 
% N : number of samples (subjects)
D = 30; T = 100; N = 120; 
data = cell(1,N);
for n=1:N
    ti = 20 + randi(T);
    data{n} = rand(D,ti);
end

sub_train = sort(randsample(size(data,2),round(size(data,2)/2)));
sub_test  = find(~ismember([1:size(data,2)]',sub_train)); 
data_train = data(sub_train);
label_train = uint32(randi(2,1,size(data_train,2)));

data_test = data(sub_test);
label_test = uint32(randi(2,1,size(data_test,2)));
% data = load('exp9_n2.txt');
% sub_train = randsample(size(data,1),round(size(data,1)/2));
% data_train = data(sub_train,2:end)';
% label_train = data(sub_train,1)';

% data_test = data(~ismember([1:size(data,1)],sub_train),2:end)';
% label_test = data(~ismember([1:size(data,1)],sub_train),1)';
%% training 
HRF_forest = ExRF_train_hankel(data_train, label_train, option);

%% testing
[label_pred, prob_pred] = ExRF_apply_hankel(data_test,HRF_forest,0);
ap = sum(label_pred == label_test)/numel(label_test);
