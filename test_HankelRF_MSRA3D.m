clear; clc;

%% Load data
% below matfile loads following variables
% data      : (3*num_joint)xT data, 557 instances, cell array
% idx_train : train indices
% idx_test  : test indices
% label_act : activity labels
% label_rep : ??
% label_sub : subject label
% num_joint : number of joints
% num_length: number of frames 1x557
% num_sample: 557
load MSRAction3D_data


num_train = sum(idx_train);
num_test  = sum(idx_test);
%% Parameters

T = inf;            % Use T samples from each data

%% Get train and test data

data_train  = data(idx_train);
data_test   = data(idx_test);
label_train = label_act(idx_train);
label_test  = label_act(idx_test);


%% Preprocessing of the data
id_refjoint = 7;

% reposition each joint wrt reference joint
for n=1:num_train
    xyz_ref = data_train{n}((id_refjoint-1)*3+1:id_refjoint*3,:);
    data_train{n} = data_train{n} - repmat(xyz_ref,[num_joint 1]);
end
for n=1:num_test
    xyz_ref = data_test{n}((id_refjoint-1)*3+1:id_refjoint*3,:);
    data_test{n} = data_test{n} - repmat(xyz_ref,[num_joint 1]);
end

% return;
%%
% calculate the derivative of the data
% for n=1:num_train
%     data_train{n} = data_train{n}(:,2:end) - data_train{n}(:,1:end-1);
% end
% for n=1:num_test
%     data_test{n} = data_test{n}(:,2:end) - data_test{n}(:,1:end-1);
% end


%% Forest Setup
option.num_w = 256; % how many random projections are generated per node.
% option.num_feature = 2;%option.num_w; % number of features
option.numTrail = 10; % how many random thresholds are tried per node
option.HankelWidth = 10; % number of Hankel matrix columns
option.maxDepth= 16;  % maximum depth of the tree
option.minCount = 8;
option.minChild = 1; 
option.mode = {'HankelPurity'};%,'DisplacementPurity'}; % training modes
option.numTree = 15; % number of trees to train
% option.numClass = 10; % number of classes 
option.rfpts = 0; % Referance point, 0--mean; 1--end point;


%% Hankel Random Forest

% train & test
hrf = ExRF_train_hankel(data_train, label_train, option);
[label_pred_hrf, prob_pred] = ExRF_apply_hankel(data_test,hrf,0);
aphrf = sum(label_pred_hrf == label_test)/numel(label_test)
