clear,clc;

%%
svcpath = '~/research/data/SVC2004/Task2';
num_users = 40;
num_sign  = 20;     % get the first 20

data   = cell(1,num_users*num_sign);
labels = zeros(1,num_users*num_sign);

n = 1;
for user_id=1:num_users
    for sign_id=1:num_sign
        fname = [svcpath '/U' num2str(user_id) 'S' num2str(sign_id) '.TXT']
        fid = fopen(fname);
        len = textscan(fid,'%d',1);
        D = textscan(fid,'%d%d%d%d%d%d%d',len);     
        fclose(fid);
        data{n} = cell2mat(D)';
        labels(n) = user_id;
        n = n+1;
        35;
    end
end

save([svcpath '/SVC2004_data.mat'],'data','labels');