%% Preparing Data
clear all; 
close all; 
clc;

% Path for the MSRC-12 data
dataPath = '../data/';
% Path to save the trained models
modelsPath = './models';
% Path to save the computed descriptors
descsPath = './descriptors';
nLevels = 1;
OL = false;
timeVar = false;
runFolderName = sprintf('%s\\gestureTrainedSVMs_LOO_%iL', modelsPath, nLevels);
if OL
    runFolderName = [runFolderName 'O'];
end
if timeVar
    runFolderName = [runFolderName 'T'];
end
if ~exist(runFolderName, 'dir')
    mkdir(runFolderName);
end

actionDataFile = sprintf('%s\\UTCMat_%iL', descsPath, nLevels);
if OL
    actionDataFile = [actionDataFile 'O'];
end
if timeVar
    actionDataFile = [actionDataFile 'T'];
end
actionDataFile = [actionDataFile '.mat'];

if ~exist(descsPath, 'dir')
    mkdir(descsPath);
end

if ~exist(actionDataFile, 'file')
    createActionData(dataPath, actionDataFile, nLevels, OL, timeVar);
end

%% Divide Data into Training and Testing
reset(RandStream.getDefaultStream);
load(actionDataFile, 'personIDs', 'actionCounts');
allPersons = sort(unique(personIDs));
nPersons = length(allPersons);
numRuns = nPersons;
personPerms = zeros(numRuns, nPersons);
trainErrs = zeros(numRuns, 1);
testErrs = zeros(numRuns, 1);
testErrLabels = cell(numRuns, 1);
testErrInds = cell(numRuns, 1);
confMats = cell(numRuns, 1);
nActionClasses = length(actionCounts);
actionRunCounts = zeros(nActionClasses, 1);
for r = 1:numRuns,
    fprintf('\nTraining model # %d\n', r);

    prepareTraingAndTestingData_LOO;
    
    %% Training Classifier
    % load trainData4SVM.mat
    % load testData4SVM.mat
    numUsedActions = numel(unique(trainLabels));
    tic
    svmCl = svmtrain(trainLabels, trainData, '-c 10 -g 1 -b 1 -q');
    [trnOuts, ~, ~] = svmpredict(trainLabels, trainData, svmCl, '-b 1');
    trnErr = sum(trnOuts ~= trainLabels) / numel(trainLabels);
    toc
    fprintf('Training the classifier is complete\n');
    fprintf('Training error = %f\n', trnErr);
    tic
    [tstOuts, accuracy, prob_estimates] = svmpredict(testLabels, testData, svmCl, '-b 1');
    toc
    tstErr = sum(tstOuts ~= testLabels) / numel(testLabels);
    testErrInds{r} = find(tstOuts ~= testLabels);
    testErrLabels{r} = testLabels(testErrInds{r});
    fprintf('Testing error = %f (%i/%i)\n', tstErr, sum(tstOuts ~= testLabels), numel(testLabels));
    trainErrs(r) = trnErr;
    testErrs(r) = tstErr;
    save(sprintf('%s\\model%02i.mat', runFolderName, r), 'svmCl');
    confMats{r} = zeros(nActionClasses);
    for a1 = 1:nActionClasses
        num1 = sum(testLabels == a1);
        if num1 == 0
            continue;
        end
        actionRunCounts(a1) = actionRunCounts(a1) + 1;
        for a2 = 1:nActionClasses
            confMats{r}(a1, a2) = sum(testLabels == a1 & tstOuts == a2);
        end
        confMats{r}(a1, :) = confMats{r}(a1, :) / num1;
    end
    clear svmCl
end
save(sprintf('%s.mat', runFolderName), 'trainErrs', 'testErrs', 'testErrInds', 'testErrLabels', 'confMats', 'actionRunCounts', 'personPerms');
errStats = @(x)(100*[min(x) max(x) median(x) mean(x) std(x)]);
disp('Training Error Statistics [min max median mean std]:');
disp(errStats(trainErrs));
disp('Testing Error Statistics [min max median mean std]:');
disp(errStats(testErrs));
repActionRunCounts = reshape(repmat(actionRunCounts, 1, nActionClasses), 1, []);
confMat = reshape(sum(cell2mat(cellfun(@(x)(reshape(x, 1, [])), confMats, 'UniformOutput', false)))./repActionRunCounts, nActionClasses, nActionClasses);
figure; imagesctx(confMat);%, [], tagset);
