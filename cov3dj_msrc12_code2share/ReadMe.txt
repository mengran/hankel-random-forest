This code implements the experiments for the COV3DJ descripotor on the MSRC-12 dataset. Some of the functions included in this code are copied (possibly after some modifications) from the code included with the MSRC-12 dataset (link below). The code should be easily adapted to other datasets.

The descriptor and experimental setups below are all explained in the following paper:
Mohamed E. Hussein, Marwan Torki, Mohammad A. Gowayyed, and Motaz El-Saban, "Human Action Recognition Using a Temporal Hierarchy of Covariance Descriptors on 3D Joint Locations," International Joint Conference on Artificial Intelligence (IJCAI), Beijing, China, August 2013

The starting scripts in the code are:

* gestureClassifyWithSVM_LOO.m for the Leave One Out setup
* gestureClassifyWithSVM_CrossSubject.m for the Cross Subject setup
* gestureClassifyWithSVM_SampleSplit.m for the Sample Split setup

Prerequisite:
=============
The code assumes the following:
1. libsvm with matlab interface is installed and added to the matlab path. This can be obtained from: http://www.csie.ntu.edu.tw/~cjlin/libsvm/
2. MSRC-12 dataset is downloaded and the path to it is set correctly at the beginning of each of the scripts above. The dataset can be obtained from: http://research.microsoft.com/en-us/um/cambridge/projects/msrc12/
3. Annotation for recognition of the MSRC-12 data is downloaded and saved in a folder named 'sepinst' that is placed directly under the folder of the MSRC-12 dataset. The annotation for recognition can be obtained from: http://www.eng.alexu.edu.eg/~mehussein/msrc12_annot4rec/index.html

Author:
Mohamed E. Hussein (mehussein@alexu.edu.eg)
