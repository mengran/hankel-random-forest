function createActionData(dataPath, outPathFile, nLevels, OL, timeVar)

sepPath = [dataPath 'sepinst/'];
seqFileNamesStr = dir([dataPath '*.csv']);
nSeqFiles = length(seqFileNamesStr);
seqFileNames = struct2cell(seqFileNamesStr);
seqFileNames = sort(seqFileNames(1, :))';
numActualInsts = zeros(nSeqFiles, 1);
numSepInsts = zeros(nSeqFiles, 1);
numUsedInsts = zeros(nSeqFiles, 1);
seqActionLabels = zeros(nSeqFiles, 1);
seqPersonIDs = zeros(nSeqFiles, 1);
actionData = [];
actionTargets = [];
actionLabels = [];
personIDs = [];
for s = 1:nSeqFiles
    fileName = seqFileNames{s}(1:end-4);
    fprintf('%3i %s: ', s, fileName);
    [XYZ, L, tagset, T] = load_file(fileName);
    if isempty(XYZ)
        fprintf('SKIPPED!\n');
        continue;
    end
    actionTarget = any(L);
    assert(sum(actionTarget) == 1);
    seqActionLabels(s) = find(actionTarget);
    numActualInsts(s) = sum(any(L, 2));
    seqPersonIDs(s) = str2double(fileName(end-1:end));
    fprintf('label=%2i, person=%2i, num gt=%2i', seqActionLabels(s), seqPersonIDs(s), numActualInsts(s));
    if exist([sepPath fileName '.sep'], 'file')
        insts = load([sepPath fileName '.sep']);
        numSepInsts(s) = size(insts, 1);
        fprintf(', num sep=%2i', numSepInsts(s));
        numInstsUsed = 0;
        for i = 1:numSepInsts(s)
            instLen = insts(i, 2) - insts(i, 1);
            if instLen >= 45 && instLen <= 192
                % instance is not too long or too short
                % this condition exclused the shortest and longest 2% of
                % the instances
                numInstsUsed = numInstsUsed + 1;
                X = XYZ(insts(i,1):insts(i,2), 1:4:end);
                Y = XYZ(insts(i,1):insts(i,2), 2:4:end);
                Z = XYZ(insts(i,1):insts(i,2), 3:4:end);
                [~, UTCMat] = computeCovMats(X, Y, Z, T(insts(i,1):insts(i,2)), nLevels, OL, timeVar);
                normVec = cell2mat(cellfun(@(x)(cell2mat(x)), reshape(UTCMat, 1, []), 'UniformOutput', false));
                normVec = normVec / (norm(normVec) + 1e-5);
%                 normVec = normalizeLin01(normVec);
                actionData = [actionData; normVec];
                actionTargets = [actionTargets; actionTarget];
            end
        end
        numUsedInsts(s) = numInstsUsed;
        actionLabels = [actionLabels; repmat(seqActionLabels(s), numInstsUsed, 1)];
        personIDs = [personIDs; repmat(seqPersonIDs(s), numInstsUsed, 1)];
        fprintf(', num used=%2i\n', numInstsUsed);
    else
        fprintf(', NO SEP FILE\n');
    end
end
actionCounts = sum(actionTargets);
fprintf('Total number of action instances available = %i, divided as follows:\n', size(actionData, 1));
for a = 1:length(actionCounts)
    fprintf('\t%3i instances from %s\n', actionCounts(a), tagset{a});
end
fprintf('Saving Action Data ...\n');
save(outPathFile, 'actionData', 'actionTargets', 'actionLabels', 'personIDs', 'seqActionLabels', 'seqPersonIDs', 'tagset', 'actionCounts', 'tagset');
fprintf('Done saving.\n');
