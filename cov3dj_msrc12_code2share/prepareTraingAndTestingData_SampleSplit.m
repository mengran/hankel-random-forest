    fprintf('Dividing data into training and testing sets\n');
    load(actionDataFile);
    trainFrac = trainFrac1/trainFrac2;
    nSamples = size(actionData, 1);
    nFeatures = size(actionData, 2);
    nActionClasses = length(actionCounts);
    trainActionCounts = zeros(nActionClasses, 1);
    for a = 1:nActionClasses
        trainActionCounts(a) = round(trainFrac * actionCounts(a));
    end
    testActionCounts = actionCounts' - trainActionCounts;
    nTrainSamples = sum(trainActionCounts);
    nTestSamples = nSamples - nTrainSamples;
    trainData = zeros(nTrainSamples, nFeatures);
    trainTargets = zeros(nTrainSamples, nActionClasses);
    trainLabels = zeros(nTrainSamples, 1);
    trainPersonIDs = zeros(nTrainSamples, 1);
    testData = zeros(nTestSamples, nFeatures);
    testTargets = zeros(nTestSamples, nActionClasses);
    testLabels = zeros(nTestSamples, 1);
    testPersonIDs = zeros(nTestSamples, 1);
    dataPermInds = randperm(nSamples);
    permActionLabels = actionLabels(dataPermInds);
    startTrainIdx = 1;
    startTestIdx = 1;

    for a = 1:nActionClasses
        fprintf('\t%d training and %d testing instances are used from action %s\n', ...
                trainActionCounts(a), testActionCounts(a), tagset{a});
        actionPermInds = find(permActionLabels == a);
        trainPermInds = dataPermInds(actionPermInds(1:trainActionCounts(a)));
        testPermInds = dataPermInds(actionPermInds(trainActionCounts(a)+1:end));
        endTrainIdx = startTrainIdx + length(trainPermInds) - 1;
        endTestIdx = startTestIdx + length(testPermInds) - 1;
        trainData(startTrainIdx:endTrainIdx, :) = actionData(trainPermInds, :);
        trainTargets(startTrainIdx:endTrainIdx, :) = actionTargets(trainPermInds, :);
        trainLabels(startTrainIdx:endTrainIdx) = actionLabels(trainPermInds);
        trainPersonIDs(startTrainIdx:endTrainIdx) = personIDs(trainPermInds);
        testData(startTestIdx:endTestIdx, :) = actionData(testPermInds, :);
        testTargets(startTestIdx:endTestIdx, :) = actionTargets(testPermInds, :);
        testLabels(startTestIdx:endTestIdx) = actionLabels(testPermInds);
        testPersonIDs(startTestIdx:endTestIdx) = personIDs(testPermInds);
        % make sure indices are not messed up in the permutation
        assert(endTrainIdx-startTrainIdx+1 == trainActionCounts(a));
        assert(endTestIdx-startTestIdx+1 == testActionCounts(a));
        assert(find(any(trainTargets(startTrainIdx:endTrainIdx, :))) == a);
        assert(find(any(testTargets(startTestIdx:endTestIdx, :))) == a);
        assert(all(trainLabels(startTrainIdx:endTrainIdx) == a));
        assert(all(testLabels(startTestIdx:endTestIdx) == a));
        assert(sum(sum(trainTargets(startTrainIdx:endTrainIdx, :))) == trainActionCounts(a));
        assert(sum(sum(testTargets(startTestIdx:endTestIdx, :))) == testActionCounts(a));
        % adjust indices for next action
        startTrainIdx = endTrainIdx + 1;
        startTestIdx = endTestIdx + 1;
    end
    % make sure arrays did not grow by mistake
    assert(nTrainSamples == size(trainData, 1));
    assert(nTestSamples == size(testData, 1));

%     fprintf('Saving training data ...\n');
%     save trainData4SVM.mat trainData trainTargets trainLabels trainPersonIDs trainActionCounts
%     fprintf('Saving testing data ...\n');
%     save testData4SVM.mat testData testTargets testLabels testPersonIDs testActionCounts

    clear actionData actionTargets 
