    fprintf('Dividing data into training and testing sets\n');
    load(actionDataFile);
    trainFrac = 0.5;
    nSamples = size(actionData, 1);
    nFeatures = size(actionData, 2);
    nActionClasses = length(actionCounts);
    trainActionCounts = zeros(nActionClasses, 1);
    nTrainPersons = ceil(trainFrac * nPersons);
    permPersons = randperm(nPersons);
    personPerms(r, :) = permPersons;
%     permPersons = personPerms(r, :);
    trainPersons = permPersons(1:nTrainPersons);
    testPersons = permPersons(nTrainPersons + 1:end);
    fprintf('Training Persons IDs:\n');
    fprintf('%2i,', trainPersons(1:end-1));
    fprintf('%2i\n', trainPersons(end));
    fprintf('Testing Persons IDs:\n');
    fprintf('%2i,', testPersons(1:end-1));
    fprintf('%2i\n', testPersons(end));
    personIndsCounts = zeros(nPersons, 1);
    for p = 1:nPersons,
        personIndsCounts(p) = sum(personIDs == allPersons(p));
    end
    nTrainSamples = sum(personIndsCounts(trainPersons));
    nTestSamples = nSamples - nTrainSamples;
    assert(sum(personIndsCounts(testPersons)) == nTestSamples);
    trainData = zeros(nTrainSamples, nFeatures);
    trainTargets = zeros(nTrainSamples, nActionClasses);
    trainLabels = zeros(nTrainSamples, 1);
    trainPersonIDs = zeros(nTrainSamples, 1);
    testData = zeros(nTestSamples, nFeatures);
    testTargets = zeros(nTestSamples, nActionClasses);
    testLabels = zeros(nTestSamples, 1);
    testPersonIDs = zeros(nTestSamples, 1);
    dataPermInds = randperm(nSamples);
    startTrainIdx = 1;
    startTestIdx = 1;
    for p = 1:nPersons,
        personInstInds = personIDs == permPersons(p);
        nPersonSamples = sum(personInstInds);
%         fprintf('Using actions of person %i for ', permPersons(p));
        if p <= nTrainPersons,
            assert(ismember(permPersons(p), trainPersons));
%             fprintf('training\n');
            endTrainIdx = startTrainIdx + nPersonSamples - 1;
            assert(endTrainIdx - startTrainIdx + 1 == personIndsCounts(permPersons(p)));
            trainData(startTrainIdx:endTrainIdx, :) = actionData(personInstInds, :);
            trainTargets(startTrainIdx:endTrainIdx, :) = actionTargets(personInstInds, :);
            trainLabels(startTrainIdx:endTrainIdx) = actionLabels(personInstInds);
            trainPersonIDs(startTrainIdx:endTrainIdx) = personIDs(personInstInds);
            trainActionCounts = trainActionCounts + sum(trainTargets(startTrainIdx:endTrainIdx, :))';
            startTrainIdx = endTrainIdx + 1;
        else
            assert(ismember(permPersons(p), testPersons));
%             fprintf('testing\n');
            endTestIdx = startTestIdx + nPersonSamples - 1;
            assert(endTestIdx - startTestIdx + 1 == personIndsCounts(permPersons(p)));
            testData(startTestIdx:endTestIdx, :) = actionData(personInstInds, :);
            testTargets(startTestIdx:endTestIdx, :) = actionTargets(personInstInds, :);
            testLabels(startTestIdx:endTestIdx) = actionLabels(personInstInds);
            testPersonIDs(startTestIdx:endTestIdx) = personIDs(personInstInds);
            startTestIdx = endTestIdx + 1;
        end
    end
    testActionCounts = actionCounts' - trainActionCounts;
    % make sure arrays did not grow by mistake
    assert(nTrainSamples == size(trainData, 1));
    assert(nTestSamples == size(testData, 1));

    clear actionData actionTargets
