function tree = treeTrain(data,label,weight_sample,weight_feature,option)
% tree = treeTrain(data,label,weight_sample,weight_feature,option)
% data[dXN]
% label[1XN]: must be integer and greater than 0
% weight_sample[1XN]
% weight_feature[1XN]
% training a single tree of extram random forest 
% by Mengran Gou

%% initialize tree structure
num_sample = size(data,2);
num_node_max = 2*num_sample-1;
thres_node = zeros(num_node_max,1,'single'); % threshold of every node
idx_feature_node = zeros(num_node_max,1,'uint32'); % index of feature used in every node
idx_data_node = cell(num_node_max,1); % index of data sample in every node
distr_node = zeros(num_node_max,max(label)); % class distribution of every node
child_node = idx_feature_node;
count_node = idx_feature_node;
depth_node = idx_feature_node;
label_node = idx_feature_node;%cell(num_node_max,1); % label of every node
leaf_node = zeros(num_node_max,1,'uint8'); % leaf node indicator
% customized parameters
randW = zeros(num_node_max,option.HankelWidth,'single'); % rand projection of every node

%% start training
node_cur = 1;
node_lchild = 2;
idx_data_node{1} = uint32(1:num_sample);
while(node_cur < node_lchild)
    idx_data_tmp = idx_data_node{node_cur};
    idx_data_node{node_cur} = [];
    label_tmp = label(idx_data_tmp);
    num_data_tmp = numel(label_tmp);
    count_node(node_cur) = num_data_tmp;
    
    pure = all(label_tmp(1)==label_tmp); % determine purity of current node
    % keep class distribution
    if (pure) 
        distr_node(node_cur,label_tmp(1)) = 1;
        label_node(node_cur) = label_tmp(1);        
    else 
        distr_node(node_cur,:) = hist(label_tmp,1:single(max(label)))/num_data_tmp;
        [~,label_node(node_cur)] = max(distr_node(node_cur,:));
    end
    % stop spliting for pure node, small node or deep enough node
    if (pure || num_data_tmp < option.minCount || depth_node(node_cur) > option.maxDepth)
        leaf_node(node_cur) = 1;
        node_cur = node_cur + 1;
        continue;
    end
    
    % split the node
    [idx_feature_tmp, thres_tmp, child_l_tmp, gain] = ...
        naiveSplitFun(data(:,idx_data_tmp), label_tmp, weight_sample(idx_data_tmp), weight_feature, option);
    count_tmp = nnz(child_l_tmp);
    if (gain >1e-10 && count_tmp >= option.minChild && num_data_tmp - count_tmp >=option.minChild)
        % valid split
        child_node(node_cur) = node_lchild;
        idx_feature_node(node_cur) = idx_feature_tmp;
        thres_node(node_cur) = thres_tmp;
        idx_data_node{node_lchild} = idx_data_tmp(child_l_tmp);
        idx_data_node{node_lchild+1} = idx_data_tmp(~child_l_tmp);
        depth_node(node_lchild:node_lchild+1) = depth_node(node_cur)+1;
        node_lchild = node_lchild + 2;        
    end
    node_cur = node_cur + 1;
end
% create output model struct
node_keep = 1:node_lchild - 1;
tree=struct('idx_feature_node',idx_feature_node(node_keep),'thres_node',thres_node(node_keep),...
  'child_node',child_node(node_keep), 'distr_node',distr_node(node_keep,:),...
  'label_node',label_node(node_keep),'count_node',count_node(node_keep),...
  'depth_node',depth_node(node_keep),'leaf_node',leaf_node(node_keep));
end
        
        
        
        