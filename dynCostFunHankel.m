function cost = dynCostFunHankel(data,w)

% data: 1xT
% w: nxL
% compute the hankel cost 1/N (wHH'w') where N is the number of rows

% w = mat2cell(w,ones(1,size(w,1)));
% data = mat2cell(repmat(data,size(w,1),1),ones(1,size(w,1)));
% c = cellfun(@conv, data, w, 'UniformOutput',0);
% c = cell2mat(c);
if size(w,1) > 1
    c = convnfft(repmat(data,size(w,1),1),w,'valid',2);
    cost = mean(c.^2,2);
else
    c = conv(data,w,'valid');
    cost = mean(c.^2);
end