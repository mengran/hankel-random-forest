clear; clc;

%% Load data
% load MHAD_data_long
% load MHAD_data_whole
load ~/research/data/SVC2004/Task2/SVC2004_data.mat
num_sample = size(data,2);

%% Preprocessing
% remove time 
% for n=1:num_sample
%     data{n}(3,:) = [];
%     data{n} = data{n}([1 2 3 4 5 6 7],:);
% end
    
    
%% Parameters
N = 250;            % N for training, N for testing
Tstart = 1;        % We will skip first several frames
Tend   = inf;            % Use T samples from each data


%% Get train and test data
idx_train = randi(num_sample,1,N);
idx_test  = randi(num_sample,1,N);
num_train = N;
num_test  = N;

data_train  = data(idx_train);
data_test   = data(idx_test);
% label_train = label_act(idx_train);
% label_test  = label_act(idx_test);
label_train = labels(idx_train);
label_test  = labels(idx_test);




%% chop the beginning of the action
% for n=1:num_train
%     data_train{n} = data_train{n}(:,Tstart:min(end,Tend));
% end
% for n=1:num_test
%     data_test{n} = data_test{n}(:,Tstart:min(end,Tend));
% end

%% reposition each joint wrt reference joint

% id_refjoint = 1
% 
% for n=1:num_train
%     xyz_ref = data_train{n}((id_refjoint-1)*3+1:id_refjoint*3,:);
%     data_train{n} = data_train{n} - repmat(xyz_ref,[num_joint 1]);
% end
% for n=1:num_test
%     xyz_ref = data_test{n}((id_refjoint-1)*3+1:id_refjoint*3,:);
%     data_test{n} = data_test{n} - repmat(xyz_ref,[num_joint 1]);
% end
%% calculate the derivative of the data
% NOTE: Using the derivative increases accuracy around 10 percent

if 1
for n=1:num_train
    data_train{n} = data_train{n}(:,2:end) - data_train{n}(:,1:end-1);
end
for n=1:num_test
    data_test{n} = data_test{n}(:,2:end) - data_test{n}(:,1:end-1);
end

end





%% Covariance features

addpath('./cov3dj_msrc12_code2share/');
addpath(genpath('./libsvm-3.20/'));

if 0
nl = 1;
ol = false;
tv = false;
covmat_data_train = [];
covmat_data_test = [];
tic;
for n=1:num_train
    X = data_train{n}(1:3:end,:)';  % nframes x njoints
    Y = data_train{n}(2:3:end,:)';
    Z = data_train{n}(3:3:end,:)';
    T = [1: size(data_train{n},2)]';
    [~, cMat] = computeCovMats(X,Y,Z,T,nl,ol,tv);     
    normVec = cell2mat(cellfun(@(x)(cell2mat(x)), reshape(cMat, 1, []), 'UniformOutput', false));
    normVec = normVec / (norm(normVec) + 1e-5);
    covmat_data_train = [covmat_data_train; normVec];
    
end
for n=1:num_test
    X = data_test{n}(1:3:end,:)';  % nframes x njoints
    Y = data_test{n}(2:3:end,:)';
    Z = data_test{n}(3:3:end,:)';
    T = [1: size(data_test{n},2)]';
    [~, cMat] = computeCovMats(X,Y,Z,T,nl,ol,tv);     
    normVec = cell2mat(cellfun(@(x)(cell2mat(x)), reshape(cMat, 1, []), 'UniformOutput', false));
    normVec = normVec / (norm(normVec) + 1e-5);
    covmat_data_test = [covmat_data_test; normVec];
    
end
toc;

end

% numSubDims = 30;
% dataDim = size(data_train{1},1);
% subDims = randi(dataDim,1,numSubDims);

subDims = [1:7];
tic
covmat_data_train = compute_covmat(data_train,subDims');
covmat_data_test = compute_covmat(data_test,subDims');
toc
%%
svmCl = svmtrain(label_train', covmat_data_train', '-c 200 -g 1 -b 1 -q');
[trnOuts, ~, ~] = svmpredict(label_train', covmat_data_train', svmCl, '-b 1');

[tstOuts, accuracy, prob_estimates] = svmpredict(label_test', covmat_data_test', svmCl, '-b 1');



%% Dynamically Lifted Data 
% We are going to pick many random regressors and lift the data using Fei's
% metric w'H'Hw. We will apply it to random subdimensions (maybe one or
% more at a time). The data dimension will be Dx1 where D is the number of
% regressors we pick.



addpath('./convfft');
% parameters
numRegressors = 200;
option.HankelWidth = 20;    % number of Hankel matrix columns
numDims = 1;         % how many joint dimensions to consider

dataDim = size(data_train{1},1);
wHHw_data_train = zeros(numRegressors,length(data_train));
wHHw_data_test  = zeros(numRegressors,length(data_test));

% pick random regressors
W = rand(numRegressors,option.HankelWidth)*2-1;
for r=1:numRegressors
    W(r,:) = W(r,:)/norm(W(r,:));
end
W = repmat(W,[numDims 1]); % replicate W

% pick random dimensions
D = randi(dataDim,1,numRegressors*numDims)';


tic;
wHHw_data_train = compute_wHHw(data_train,D,W);
wHHw_data_test = compute_wHHw(data_test,D,W);
toc;

%%
svmCl2 = svmtrain(label_train', wHHw_data_train', '-c 500 -g 1 -b 1 -q');
[trnOuts, ~, ~] = svmpredict(label_train', wHHw_data_train', svmCl2, '-b 1');

[tstOuts, accuracy, prob_estimates] = svmpredict(label_test', wHHw_data_test', svmCl2, '-b 1');
