function forest = ExRF_train(data, label, option)
% forest = ExRF_train(data, label, option)
% data[dXN]--input data
% label[1XN]
% Train extrem random forest
% by Mengran Gou


% get additional parameters and fill in remaining parameters
% dfs={ 'M',10, 'H',[], 'N1',[], 'F1',[], 'split','gini', 'minCount',1, ...
%   'minChild',1, 'maxDepth',64, 'dWts',[], 'fWts',[], 'num_w', 256,...
%   'HankelWidth', 8};
% [M,H,N1,F1,splitStr,minCount,minChild,maxDepth,dWts,fWts,num_w,HankelWidth] = ...
%   getPrmDflt(varargin,dfs);
num_sample_tmp = [];
num_feature_tmp = [];
weight_sample = [];
weight_feature = [];
split = [];

num_sample  = size(data,2);
num_feature = size(data,1);
splitStr = 'gini';
% robust input test
if(isempty(num_sample_tmp)), num_sample_tmp=num_sample; end; 
num_sample_tmp=min(num_sample,num_sample_tmp);
% if(isempty(num_feature_tmp)), num_feature_tmp=round(sqrt(option.num_feature)); end; 
% num_feature_tmp=min(option.num_feature,num_feature_tmp);
if(isempty(weight_sample)), weight_sample=ones(1,num_sample,'single'); end; 
weight_sample=weight_sample/sum(weight_sample);
if(isempty(weight_feature)), weight_feature=ones(1,num_feature,'single'); end; 
weight_feature=weight_feature/sum(weight_feature);
split=find(strcmpi(splitStr,{'gini','entropy','twoing'}))-1;
if(isempty(split)), error('unknown splitting criteria: %s',splitStr); end

if ~isa(data,'single'), data = single(data); end
if ~isa(weight_feature,'single'), weight_feature = single(weight_feature); end
if ~isa(weight_sample,'single'),weight_sample = single(weight_sample); end
if ~isa(label,'uint32'), label = uint32(label); end

for i = 1:option.numTree
    % Boostrap
    idx_BTrap = randi(num_sample,num_sample,1);
    weight_sample = hist(idx_BTrap,1:num_sample);
    weight_sample = weight_sample./num_sample;
    
    % train single tree
    tree = treeTrain(data,label,weight_sample,weight_feature,option);
    if (i==1) 
        forest = tree(ones(option.numTree,1)); 
    else
        forest(i) = tree;
    end
        
end



