clear; clc;
addpath(genpath('~/research/code/matlab/common/hankel/'));

%% Load data
% load MHAD_data_long
load MHAD_data_whole
num_sample = size(data,2);

%% Parameters
N = 200;            % N for training, N for testing
Tstart = 1;        % We will skip first several frames
T = 60;            % Use T samples from each data


%% Get train and test data

data_train  = cell(1,N);
ddata_train  = cell(1,N);   % derivative of the data
label_train = zeros(1,N);
data_test  = cell(1,N);
ddata_test  = cell(1,N);
label_test = zeros(1,N);

for n=1:N
    i = randi(num_sample,1);
%     data_train{n}   = data{i}(:,Tstart:Tstart+T-1);
    data_train{n}   = data{i};
    ddata_train{n}  = data_train{n}(:,2:end) - data_train{n}(:,1:end-1);
    label_train(n)  = label_act(i);
end

for n=1:N
    i = randi(num_sample,1);
%     data_test{n}  = data{i}(:,Tstart:Tstart+T-1);
    data_test{n}  = data{i};
    ddata_test{n}  = data_test{n}(:,2:end) - data_test{n}(:,1:end-1);
    label_test(n) = label_act(i);
end

%% Forest Setup
option.num_w = 256; % how many random projections are generated per node.
% option.num_feature = 2;%option.num_w; % number of features
option.numTrail = 10; % how many random thresholds are tried per node
option.HankelWidth = 10; % number of Hankel matrix columns
option.maxDepth= 16;  % maximum depth of the tree
option.minCount = 8;
option.minChild = 1; 
option.mode = {'HankelPurity'};%,'DisplacementPurity'}; % training modes
option.numTree = 15; % number of trees to train
% option.numClass = 10; % number of classes 
option.rfpts = 0; % Referance point, 0--mean; 1--end point;

%% Vanilla Random Forest
if 0
data_train_concat = zeros(prod(size(data_train{1})),length(data_train));
data_test_concat = zeros(prod(size(data_test{1})),length(data_test));
for n=1:N
    data_train_concat(:,n) = data_train{n}(:);
    data_test_concat(:,n) = data_test{n}(:);
end

% train & test
vrf = ExRF_train(data_train_concat, label_train, option);
[label_pred_vanilla, prob_pred] = ExRF_apply(data_test_concat,vrf,0);
apvrf = sum(label_pred_vanilla == label_test)/numel(label_test)
end

%% Hankel Random Forest

% train & test
hrf = ExRF_train_hankel(ddata_train, label_train, option);
[label_pred_hankel, prob_pred] = ExRF_apply_hankel(ddata_test,hrf,0);
aphrf = sum(label_pred_hankel == label_test)/numel(label_test)

return;
%% Dynamically Lifted Data Vanilla Random Forest
% We are going to pick many random regressors and lift the data using Fei's
% metric w'H'Hw. We will apply it to random subdimensions (maybe one or
% more at a time). The data dimension will be Dx1 where D is the number of
% regressors we pick.
numRegressor = 200;
dataDim = size(data_train{1},1);
data_train_wHHw_concat = zeros(numRegressor,length(data_train));
data_test_wHHw_concat  = zeros(numRegressor,length(data_test));

for r=1:numRegressor
    % pick random regressor
    w = rand(1,option.HankelWidth)*2-1;
    w = w/norm(w);
    
    % pick an input dimension
    d = randi(dataDim);
    for n=1:N
        data_train_wHHw_concat(r,n) = dynCostFunHankel(ddata_train{n}(d,:),w);
        data_test_wHHw_concat(r,n) = dynCostFunHankel(ddata_test{n}(d,:),w);
    end
end

% apply training and testing to lifted data
dlvrf = ExRF_train(data_train_wHHw_concat, label_train, option);
[label_pred_dl_vanilla, prob_pred] = ExRF_apply(data_test_wHHw_concat,dlvrf,0);
apdlvrf = sum(label_pred_dl_vanilla == label_test)/numel(label_test)


return;
%% Hankelized Data Vanilla Random Forest
% NOTE: Remember this idea came from talking with Octavia. She suggested
% hankelizing the data and trying it on regular RF might be an interesting
% comparison. That is what we are doing here. 

data_train_hankelized = cell(1,length(ddata_train));
data_test_hankelized = cell(1,length(ddata_train));
label_train_hankelized = cell(1,length(ddata_test));
label_test_hankelized = cell(1,length(ddata_test));
num_train_hankelized = zeros(1,length(ddata_train));
num_test_hankelized = zeros(1,length(ddata_test));

% this loop makes hankels for each dimension. hankels have fixed column
% number which corresponds to our time window.
for n=1:length(ddata_train)
    data_n = [];
    
    for d=1:size(ddata_train{n},1)
        data_n = [data_n; hankel_mo(ddata_train{n}(d,:),[0 option.HankelWidth])'];
    end
    
    data_train_hankelized{n} = data_n;
    label_train_hankelized{n} = repmat(label_train(n),[1 size(data_n,2)]);
    num_train_hankelized(n) = size(data_n,2);
end

for n=1:length(data_test)
    data_n = [];
    
    for d=1:size(data_test{n},1)
        data_n = [data_n; hankel_mo(ddata_test{n}(d,:),[0 option.HankelWidth])'];
    end
    
    data_test_hankelized{n} = data_n;
%     label_test_hankelized{n} = repmat(label_train(n),[1 size(data_n,2)]);
    num_test_hankelized(n) = size(data_n,2);
end


data_train_hankelized_concat = cell2mat(data_train_hankelized);
label_train_hankelized = cell2mat(label_train_hankelized);
data_test_hankelized_concat = cell2mat(data_test_hankelized);


% train & test
hvrf = ExRF_train(data_train_hankelized_concat, label_train_hankelized, option);
[label_pred_hankelized_concat, prob_pred_hankelized_concat] = ExRF_apply(data_test_hankelized_concat,hvrf,0);

% apply aggregated majority vote
label_pred_hankelized = zeros(1,length(data_test));
cumnum_test_hankelized = cumsum(num_test_hankelized);
cumnum_test_hankelized = [0 cumnum_test_hankelized];
for n=1:length(data_test)
    nstart = cumnum_test_hankelized(n)+1;
    nend = cumnum_test_hankelized(n+1);
    
    % find the majoriy label in sublabels
    lbl = label_pred_hankelized_concat(nstart:nend);
    [m,f,c] = mode(lbl);    % nice function to find most frequent number
    label_pred_hankelized(n) = m;
end

aphvrf = sum(label_pred_hankelized == label_test)/numel(label_test)
