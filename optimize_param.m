function [err_funvals,funvals] = optimize_param(func,funcparams,defopts,optname,optvals)

numruns = 10;

lenopt = length(optvals);
funvals = zeros(numruns,lenopt);

display(['optimizing ' optname]);
if ~isfield(defopts,optname)
    error(['No such option: ' optname]);
end

tmpopts = defopts;

for j=1:lenopt
    for i=1:numruns        
        tmpopts =  setfield(tmpopts,optname,optvals(j));
        
        % test the function 
        [accuracy,tstOuts] = func(funcparams{:},tmpopts);
        funvals(i,j) = accuracy(1);
        
        fprintf('%s = %g  run = %g accuracy = %g\n',optname,optvals(j),i,accuracy(1))
        35;
    end
end


mean_funvals = mean(funvals,1);
std_funvals  = std(funvals,[],1);
err_funvals  = [mean_funvals; std_funvals/sqrt(numruns)];




