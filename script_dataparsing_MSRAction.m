clc
clear 
%% Following the protocol from Wang, et. al CVPR12
datafolder = 'data_screen';
name_keep = [];
fp = fopen('JiangExperimentFileList.txt');
tmpname = fscanf(fp,'%s',1);
n = 1;
while ~isempty(tmpname)
    name_keep{n} = tmpname;
    n = n+1;
    tmpname = fscanf(fp,'%s',1);
end
fclose(fp);

label_act = [];
label_sub = [];
label_rep = [];
data_x = [];
data_y = [];
data_z = [];
offset = [];
num_frame = [];
num_joint = 20;
data = cell(1,numel(name_keep));
for b = 1:numel(name_keep)
    b
    txt_name = [name_keep{b} '_skeleton.txt'];
    us_loc = strfind(txt_name,'_');
    act = str2double(txt_name(2:us_loc(1)-1));
    sub = str2double(txt_name(us_loc(1)+2:us_loc(2)-1));
    rep = str2double(txt_name(us_loc(2)+2:us_loc(3)-1));
    tmpdata = load(fullfile(datafolder,txt_name));

    num_frame = size(tmpdata,1)/num_joint;
    confidence = tmpdata(:,4);
    confidence = repmat(confidence',3,1);
    tmpdata = tmpdata(:,1:3)';
    X = tmpdata(1,:);    
    Z = 400-tmpdata(2,:);
    Y = tmpdata(3,:)/4;
    data_tmp = reshape([X;Y;Z],3*num_joint,num_frame);
    confidence = reshape(confidence,3*num_joint,num_frame);
    data{b} = data_tmp;
    label_act(b) = act;
    label_sub(b) = sub;
    label_rep(b) = rep;
end
num_length = cellfun(@size,data,'UniformOutput',0);
num_length = cell2mat(num_length');
num_length = num_length(:,2)';
%%
sub_train = [1 3 5 7 9];
sub_test = [2 4 6 8 10];
idx_train = ismember(label_sub,sub_train);
idx_test = ismember(label_sub,sub_test);
save('MSRAction3D_data.mat','data','label_act','label_rep','label_sub','num_joint','idx_train','idx_test','num_length');