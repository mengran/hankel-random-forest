function [idxf, w, thres, child_l, gain] = naiveSplitFunHankel(data, label, weight_sample, weight_feature, option)
% data[1XN] cell

N = size(data,2);
idxf_tmp = wswor(weight_feature,round(sqrt(size(data{1},1))),4); % feature/dimension subsample
data_slice = cell(size(data));
for n=1:N
    data_slice{n} = data{n}(idxf_tmp,:);
end

w = rand(option.num_w,option.HankelWidth)*2-1;
w = bsxfun(@times,w,1./sqrt(sum(w.^2,2))); %L2 normalization for each w
subdims = length(idxf_tmp);
data_tmp = single(zeros(N,subdims*option.num_w));
for n = 1:N
    for d = 1:subdims
%         data_tmp(n,d) = norm(data_slice{n}(d,:));  % TOOD: put your norm here
%         data_tmp(n,d) = dynCostFunHankel(data_slice{n}(d,:),w); 
        data_tmp(n,(d-1)*option.num_w+1:d*option.num_w) = dynCostFunHankel(data_slice{n}(d,:),w); 
    end
end

% data_tmp = single(rand(1,N)');


[~,order_tmp] = sort(data_tmp);
order_tmp = uint32(order_tmp - 1);
% entropy/gini based split
[idf,thres,gain] = forestFindThr_matlab(data_tmp,label,weight_sample,order_tmp,max(label),0);
% idxf = idxf_tmp(idf);
% back projected to obtain w and dimension id
n_w = mod(idf,option.num_w);
if n_w == 0
    n_w = option.num_w;
end
w = w(n_w,:);
idxf = idxf_tmp(ceil(idf/option.num_w));
% child_l = data(idxf,:) < thres;   % TODO: Why is this line is here?
child_l = data_tmp(:,idf) < thres;
end