function [accuracy,tstOuts] = traintest_covmat(data_train,label_train,data_test,label_test,options)


addpath('./cov3dj_msrc12_code2share/');
addpath(genpath('./libsvm-3.20/'));

num_train = size(data_train,2);
num_test = size(data_test,2);



if options.UseDerivative
    for n=1:num_train
        data_train{n} = data_train{n}(:,2:end) - data_train{n}(:,1:end-1);
    end
    for n=1:num_test
        data_test{n} = data_test{n}(:,2:end) - data_test{n}(:,1:end-1);
    end
    
end



nl = options.NumLevels;
ol = options.UseOverlap;
tv = false;
covmat_data_train = [];
covmat_data_test = [];
% tic;
for n=1:num_train
    X = data_train{n}(1:3:end,:)';  % nframes x njoints
    Y = data_train{n}(2:3:end,:)';
    Z = data_train{n}(3:3:end,:)';
    T = [1: size(data_train{n},2)]';
    [~, cMat] = computeCovMats(X,Y,Z,T,nl,ol,tv);     
    normVec = cell2mat(cellfun(@(x)(cell2mat(x)), reshape(cMat, 1, []), 'UniformOutput', false));
    normVec = normVec / (norm(normVec) + 1e-5);
    covmat_data_train = [covmat_data_train; normVec];
    
end
for n=1:num_test
    X = data_test{n}(1:3:end,:)';  % nframes x njoints
    Y = data_test{n}(2:3:end,:)';
    Z = data_test{n}(3:3:end,:)';
    T = [1: size(data_test{n},2)]';
    [~, cMat] = computeCovMats(X,Y,Z,T,nl,ol,tv);     
    normVec = cell2mat(cellfun(@(x)(cell2mat(x)), reshape(cMat, 1, []), 'UniformOutput', false));
    normVec = normVec / (norm(normVec) + 1e-5);
    covmat_data_test = [covmat_data_test; normVec];
    
end
% toc;

%%
svmCl = svmtrain(label_train', covmat_data_train, '-c 200 -g 1 -b 1 -q');
% svmCl = svmtrain(label_train', covmat_data_train, '-c 200 -b 1 -q');
[tstOuts, accuracy, prob_estimates] = svmpredict(label_test', covmat_data_test, svmCl, '-b 1 -q');
35;
