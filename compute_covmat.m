function feat_covmat = compute_covmat(data,subdims)

% compute covmat data. Cha version for any dimension of data. 
numData = size(data,2);
numSubDims = size(subdims,1);
numDims = numSubDims*(numSubDims+1)/2;

% get the indices for upper triangular part
triuind = triu(ones(numSubDims));
triuind = sparse(boolean(triuind(:)));

feat_covmat = zeros(numDims,numData);
for n=1:numData
    C = cov(double(data{n})');
    f = C(triuind);
    f = f / (norm(f) + 1e-5);
    feat_covmat(:,n) = f;
end