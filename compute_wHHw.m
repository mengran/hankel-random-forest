function feat_wHHw = compute_wHHw(data,subdims,w)
% data:    1xN cell
% subdims: MxK indices of dimensions to for subsampling 
% w:       MxT regressors. 
%
%          M: number of regressors 
%          K: number of dimensions of block hankel (eg for (x,y) K=2)
%          T: length of hankel window

[numRegressors, numDims] = size(subdims);
numData = size(data,2);
lenData = cellfun(@(d) size(d,2),data);
feat_wHHw = zeros(numRegressors,numData);

if 0
% take the fft of all data
DATA = cell(1,numData);
dim = 2;
% Following code is from convnfft.
% fft setup
ifun = @(m,n) n:m;
% faster FFT if the dimension is power of 2
lfftfun = @(l) 2^nextpow2(l);


% fft length based on max length (this is not the smartest way)

% m = size(data{i},dim);
m = max(lenData);
n = size(w,dim);
l = lfftfun(m+n-1);
W = fft(w,l,dim);
%
subs(1:ndims(data)) = {':'};

subs{dim} = ifun(m,n);

tic;
% compute fft
DATA = cellfun(@(data) fft(data,l,dim),data,'UniformOutput',false);

% compute multiplication
DATA = cellfun(@(data) (data(subdims,:).*W),DATA,'UniformOutput',false);
toc;

for i=1:numData    
    DATA{i} = fft(data{i},l,dim);
end


end



options.Power2Flag = true;
% options.weightedMean = false; % adding weights for the elements of wHH'w'

    
for n=1:numData
    % the following convolution assumes one hankel on top of each other
    % as H = [H1;H2; ...] so we can do manipulations easily
    
    C = convnfft(double(data{n}(subdims(:),:)),fliplr(w),'valid',2,options);
    if 1
        q = mean(abs(data{n}),1);
        thresh = 0.2;
        q = (q > thresh);
        q = q(floor(size(w,2)/2):end-ceil(size(w,2)/2));    % chop ends
        f = mean(C(:,q).^2,2);
    else 
        f = mean(C.^2,2);
    end    
    f = reshape(f,[numRegressors numDims]);
    f = mean(f,2);
    f = f / (norm(f) + 1e-5);
    feat_wHHw(:,n) = f;
end
 