function [idxf, thres, child_l, gain] = naiveSplitFun(data, label, weight_sample, weight_feature, option)
% data[dXN]
idxf_tmp = wswor(weight_feature,round(sqrt(size(data,1))),4); % feature subsample 
data_tmp = data(idxf_tmp,:)';
[~,order_tmp] = sort(data_tmp);
order_tmp = uint32(order_tmp - 1);
% entropy/gini based split
[idf,thres,gain] = forestFindThr_matlab(data_tmp,label,weight_sample,order_tmp,max(label),0);
idxf = idxf_tmp(idf);
child_l = data(idxf,:) < thres;
end