function [accuracy,tstOuts] = traintest_wHHw(data_train,label_train,data_test,label_test,options)

addpath('./convfft');

num_train = size(data_train,2);
num_test = size(data_test,2);
dataDim = size(data_train{1},1);

if options.UseDerivative
    for n=1:num_train
        data_train{n} = data_train{n}(:,2:end) - data_train{n}(:,1:end-1);
    end
    for n=1:num_test
        data_test{n} = data_test{n}(:,2:end) - data_test{n}(:,1:end-1);
    end
    
end


% parameters
numRegressors = options.NumRegressors;
hankelWidth   = options.HankelWidth;    % number of Hankel matrix columns
numDims       = options.NumDims;         % how many joint dimensions to consider



% pick random regressors
W = rand(numRegressors,hankelWidth)*2-1;
for r=1:numRegressors
    W(r,:) = W(r,:)/norm(W(r,:));
end
W = repmat(W,[numDims 1]); % replicate W

% pick random dimensions
D = randi(dataDim,1,numRegressors*numDims)';



wHHw_data_train = compute_wHHw(data_train,D,W);
wHHw_data_test = compute_wHHw(data_test,D,W);


%%
svmCl2 = svmtrain(label_train', wHHw_data_train', '-c 500 -g 1 -b 1 -q');
% [trnOuts, ~, ~] = svmpredict(label_train', wHHw_data_train', svmCl2, '-b 1');
[tstOuts, accuracy, prob_estimates] = svmpredict(label_test', wHHw_data_test', svmCl2, '-b 1 -q');
35;