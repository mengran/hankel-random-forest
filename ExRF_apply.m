function [label, prob] = ExRF_apply(data,forest,best)
% data[dXN]
% forest
% best[0] if true use single best prediction pre tree
num_sample = size(data,2);
num_tree = numel(forest);
num_class = size(forest(1).distr_node,2);
label = zeros(num_sample,num_tree);
prob = zeros(num_sample,num_class);
for i = 1:num_tree
    tree = forest(i);
    idx_leaf = naiveApplyFun(data,tree);
    if best        
        label(:,i) = tree.label_node(idx_leaf);
    else 
        prob = prob + tree.distr_node(idx_leaf,:);
    end
end
if best
    prob = histc(label',1:num_class)';
end
[~,label] = max(prob,[],2);
prob = prob/num_tree;

label = label';
prob = prob';