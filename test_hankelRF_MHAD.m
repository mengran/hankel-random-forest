clear; clc;

%% Load data
% load MHAD_data_long
load MHAD_data_whole
num_sample = size(data,2);

%% Parameters
N = 250;            % N for training, N for testing
Tstart = 1;        % We will skip first several frames
Tend   = inf;            % Use T samples from each data


%% Get train and test data
idx_train = randi(num_sample,1,N);
idx_test  = randi(num_sample,1,N);
num_train = N;
num_test  = N;

data_train  = data(idx_train);
data_test   = data(idx_test);
label_train = label_act(idx_train);
label_test  = label_act(idx_test);




%% chop the beginning of the action
% for n=1:num_train
%     data_train{n} = data_train{n}(:,Tstart:min(end,Tend));
% end
% for n=1:num_test
%     data_test{n} = data_test{n}(:,Tstart:min(end,Tend));
% end

%% reposition each joint wrt reference joint

% id_refjoint = 1
% 
% for n=1:num_train
%     xyz_ref = data_train{n}((id_refjoint-1)*3+1:id_refjoint*3,:);
%     data_train{n} = data_train{n} - repmat(xyz_ref,[num_joint 1]);
% end
% for n=1:num_test
%     xyz_ref = data_test{n}((id_refjoint-1)*3+1:id_refjoint*3,:);
%     data_test{n} = data_test{n} - repmat(xyz_ref,[num_joint 1]);
% end
%% calculate the derivative of the data
% NOTE: Using the derivative increases accuracy around 10 percent

if 1
for n=1:num_train
    data_train{n} = data_train{n}(:,2:end) - data_train{n}(:,1:end-1);
end
for n=1:num_test
    data_test{n} = data_test{n}(:,2:end) - data_test{n}(:,1:end-1);
end

end

%% Forest Setup
option.num_w = 10; % how many random projections are generated per node.
% option.num_feature = 2;%option.num_w; % number of features
option.numTrail = 10; % how many random thresholds are tried per node
option.HankelWidth = 10; % number of Hankel matrix columns
option.maxDepth= 16;  % maximum depth of the tree
option.minCount = 8;
option.minChild = 1; 
option.mode = {'HankelPurity'};%,'DisplacementPurity'}; % training modes
option.numTree = 15; % number of trees to train
% option.numClass = 10; % number of classes 
option.rfpts = 0; % Referance point, 0--mean; 1--end point;


%% Hankel Random Forest
if 1
% train & test
hrf = ExRF_train_hankel(data_train, label_train, option);
[label_pred_hankel, prob_pred] = ExRF_apply_hankel(data_test,hrf,0);
aphrf = sum(label_pred_hankel == label_test)/numel(label_test)

end
