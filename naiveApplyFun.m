function idx_leaf = naiveApplyFun(data,tree)
%data[dXN]

num_sample = size(data,2);
idx_leaf = zeros(1,num_sample,'uint32');
node_cur = 1;
node_lchild = 2;
idx_data = cell(1,length(tree.child_node));

idx_data{1} = 1:num_sample;
while node_cur < node_lchild
    idx_tmp = idx_data{node_cur};
    if tree.child_node(node_cur)==0 % leaf node
        idx_leaf(idx_tmp) = node_cur;
        node_cur = node_cur + 1;
        continue;
    end
    data_tmp = data(:,idx_tmp); % get data in current node
    % apply the simple classifier
    idx_left_tmp = data_tmp(tree.idx_feature_node(node_cur),:) < tree.thres_node(node_cur);
    % keep results in child node
    idx_data{node_lchild} = idx_tmp(idx_left_tmp);
    idx_data{node_lchild+1} = idx_tmp(~idx_left_tmp);
    node_cur = node_cur + 1;
    node_lchild = node_lchild + 2;
end
    