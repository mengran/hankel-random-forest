function [idf,thres,gain] = forestFindThr_matlab(data,label,weight,order,num_class,split)
% data: Nxd
if min(order)<1 % robust input
    order = order + 1;
end
weight_total = sum(weight);
% initialize
prob = 0;
if split == 0 % gini
    for l = 1:num_class
        prob_tmp = sum(weight(label==l))/weight_total;
        prob = prob + prob_tmp^2;
    end
    value_ini = 1 - prob;
end
value_best = value_ini;
idf = 1;
thres = 0;
% loop over features and then thresholds
for f = 1:size(data,2)
    order_tmp = order(:,f);
    for n = 2:size(data,1)
        weight_l = weight(order_tmp(1:n-1));
        label_l = label(order_tmp(1:n-1));
        weight_l_total = sum(weight_l);
        weight_r = weight(order_tmp(n:end));
        label_r = label(order_tmp(n:end));
        weight_r_total = sum(weight_r);
        prob_r = 0;
        prob_l = 0;
        if split == 0 %gini
            for l = 1:num_class
                prob_l_tmp = sum(weight_l(label_l==l))/weight_l_total;
                prob_l = prob_l + prob_l_tmp^2;
                prob_r_tmp = sum(weight_r(label_r==l))/weight_r_total;
                prob_r = prob_r + prob_r_tmp^2;
            end
            gini_l = 1-prob_l;
            gini_r = 1-prob_r;
            value = gini_l*weight_l_total/weight_total + ...
                    gini_r*weight_r_total/weight_total;
        end
        if value < value_best
            value_best = value;
            idf = f;
            thres = (data(order_tmp(n-1),f) + data(order_tmp(n),f))/2;
        end
    end
end
gain = value_ini - value_best;
            
            
            
            
            